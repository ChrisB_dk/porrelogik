#!/usr/bin/env node
'use strict'

/**
  *  check some system stuff
  */

// get some helpers //
const os = require( 'os' )
const fs = require( 'fs' )

// export the info we got //
module.exports = {
  isFile: ( file ) => {
    var stats = fs.statSync( file )
    return stats.isFile()
  },
  isModule: ( name ) => {
    let counter = 0
    module.paths.forEach( ( nodeModulesPath ) => {
      var file = nodeModulesPath + '/' + name
      var stats = fs.statSync( file )
      console.log( 'file:' + file )
/*
      if ( stats.isDirectory() ) {
        return true
      } else {
        counter++
        if ( counter === module.paths.lenght ) {
          return false
        }
      }
*/
    })
  },
  isLoaded: ( name ) => {
    return false
  },
  getArch: () => {
    return os.arch()
  },
  getHost: () => {
    return os.hostname()
  }

}
