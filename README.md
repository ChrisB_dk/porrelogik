## [ READMÆÆH ] ##

The basic core logic for controlling the robots we are building for this years
MakerCity Festival @ Odense Havnekulturfestival.
Trying to make everything after the ES6 standart.

## [ TODU ] ##

- Error handling

## [ PORRELOGIK ] ##

- Direct control of motors, pumps, lights and other moving parts.
- PWM control.
- Solid state relays.
- H-Bridge & mosfet drivers.
- Analog or digital readings from sensors.
- Proximity & distance sensors.
- Light & sounds sensors.
- Movement sensors.
- Camera streaming.
- Face & object tracking.
- Wireless access over local network & ad-hoc.
- Browser compatible user interface.

## [ HÅRDVEJR ] ##

- MCP23S17 : 16 x digital in/outputs via SPI
- MCP3008 : 8 x 10bit analog inputs via SPI
- Camera : RaspiCam or USB  

## [ PLATTEFORMER ] ##

- Raspberry PI 2/3
- A Linux distribution that supports node.js

## [ LICENSE ] ##

Copyright 2016 Bjørn Christiansen : ChrisB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
