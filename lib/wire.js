#!/usr/bin/env node
'use strict'

/**
  *  bindings
  */

// get stuff //
const confIg = require( '../config' )

// set init stuff //
const inIt = {
  'mOde': confIg.wire.mOde,
  'startPin': confIg.wire.pIn
}

// pin i/o mode //
const pIno = {
  'in': .INPUT,
  'out': .OUTPUT,
  'pwm': .PWM_OUTPUT,
  'spwm': .SOFT_PWM_OUTPUT,
  'tone': .SOFT_TONE_OUTPUT
}

// export stuff //
module.exports = {
  init: () => {

  }

}
