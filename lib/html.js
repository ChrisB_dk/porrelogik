#!/usr/bin/env node
'use strict'

/**
  * HTML site generator ( code )
  *
  */

// import stuff we need //
const fs = require( 'fs' )
const info = require( './info' )
const config = require( '../config' )

const filePath = config.html.pAth

/*
info.isModule( 'socket.io', ( exists ) => {
  if ( exists ) {
  }
})
*/

// make script checker //
var hasScript = () => {
  var scrpCode = '<script src="js/' + + '"></script>\n'
  var jQuery = '<script src="js/jquery.min.js"></script>\n'
  var bootStrap = '<script src="js/bootstrap.min.js"></script>\n'
  var socketIo = '<script src="socket.io/socket.io.js"></script>\n'
  var myScript = '<script src="js/' + config.html.sCrpt + '"></script>\n'

}

// hardcoded html //
const html = {
  head: [
    '<!DOCTYPE html>',
  	'<html lang="da-DK">',
  	'  <meta charset="utf-8" />',
  	'  <meta http-equiv="x-ua-compatible" content="ie=edge" />',
  	'  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />',
  	'  <meta name="description" content="" />',
  	'  <meta name="author" content="" />',
  	'  <title>'+ config.html.tItle +'</title>',
  	'  <link href="favicon.ico" rel="icon" />',
  	'  <link href="css/bootstrap.min.css" rel="stylesheet" />',
  	'  <link href="css/main.css" rel="stylesheet" />'
  ],
  style: () => {
    var htmlCode = ''


    return htmlCode
  },
  body: ( htmlFile ) => {
  	var htmlCode = '  <body>\n'
  	var raw = fs.readFileSync( filePath + htmlFile, 'utf8' ).toString().split('\n')
  	for ( let i in raw ) {
  		htmlCode += '    ' + raw[i] + '\n'
  	}
  	return htmlCode
  },
  script: () => {
    var htmlCode = ''

    htmlCode += '    <script src="js/jquery.min.js"></script>\n'
    htmlCode += '    <script src="js/bootstrap.min.js"></script>\n'
/*
    if ( info.isModule( 'socket.io' ) && info.isLoaded( 'socket.io' ) ) {
      htmlCode += '    <script src="socket.io/socket.io.js"></script>\n'
    }
*/
    if ( info.isFile( filePath + config.html.sCript ) && config.html.sCript ) {
      htmlCode += '    <script src="js/' + config.html.sCript + '"></script>\n'
    }

    return htmlCode
  },
  foot: [
    '  </body>',
    '</html>'
  ]

}


// export html render function //
module.exports = {
  render: () => {
    var site = ''
    for ( let i in html.head ) {
      site += html.head[i] + '\n'
    }
    // render stylesheet //

    // render body //
    site += html.body( config.html.bOdy )

    // render javascripts //
    site += html.script()

    for ( let i in html.foot ) {
      site += html.foot[i] + '\n'
    }
    return site
  }
}
