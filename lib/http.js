#!/usr/bin/env node
/**
  * @module http
  *	@description
	* a http server without 3th party modules. except socket.io ( for now )
  */
'use strict'

// import dependencies //
const http = require( 'http' )
const server = http.createServer()
const fs = require( 'fs' )
const path = require( 'path' )
const html = require( './html' )
const config = require( '../config' )

/**
	*	@method contentType
	* @param ext
	* @description
	*	returns the correct content type for the header
	*/
const contentType = ( ext ) => {
	var type
	switch ( ext ) {
		case '.html':
			type = 'text/html'
			break
		case '.ico':
			type = 'image/x-icon'
			break
		case '.css':
			type = 'text/css'
			break
		case '.js':
			type = 'text/javascript'
			break
		case '.jpg':
			type = 'image/jpeg'
			break
		case '.jpeg':
			type = 'image/jpeg'
			break
		case '.png':
			type = 'image/png'
			break
		case '.svg':
			type = 'text/xml'
			break
		default:
			type = 'text/plain'
			break
	}
	return { 'Content-Type' : type }
}


module.exports = {

	/**
		* @method init
		* @description
		* start server our html site
		*/
  init: () => {
    const port = config.http.pOrt
    const host = config.http.hOst
		const filePath = config.html.pAth
    server.on( 'request', ( request, response ) => {
			console.log( 'HTTP request: ' + request.url )
			var type = path.parse( request.url )
			switch ( request.url ) {
				case '/' :
					response.writeHead( 200, contentType( '.html' ) )
					response.end( html.render() )
					break
				case '/favicon.ico' :
					filePath += type.base
					fs.stat( filePath, ( error, stats ) => {
						if ( error ) {
							throw error
							// http_error( response, error )
							// return
						}
						fs.readFile( filePath, ( error, data ) => {
		          if ( error ) {
		            throw error
								// http_error( response, error )
								// return
		          }
							response.writeHead( 200,  contentType( type.ext ) )
							response.end( data )
						} )
					} )
					break
				case '/css/' + config.html.sTyle :
					config.html.pAth + type.base

				case '/js/' + config.html.sCript :
					config.html.pAth + type.base

				default:
					config.html.pAth + 'vendor' + type.dir + '/' + type.base
					fs.stat( filePath, ( error, stats ) => {
						if ( error ) {
							throw error
							// http_error( response, error )
							// return
						}
						fs.readFile( filePath, ( error, data ) => {
							if ( error ) {
								throw error
								// http_error( response, error )
								// return
							}
							response.writeHead( 200,  contentType( type.ext ) )
							response.end( data )
						} )
				} )

			}
		} )
		server.listen( port, host, () => {
			console.log( 'server running on %s : %d', host, port )
		})
	},

	server: server

}

/*
        var type = path.parse( request.url )
        if ( type.name != 'main' ) {
          filePath += 'vendor' + type.dir + '/'
					filePath += type.base
        } else {
          filePath += type.base
				}
				fs.readFile( filePath, ( error, data ) => {
          if ( error ) {
            throw error
          }
          response.writeHead( 200,  contentType( type.ext ) )
          response.end( data )
        })
      }
    })
  },
  server: server
}
*/
