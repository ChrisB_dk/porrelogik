#!/usr/bin/env node
'use strict'

/**
  * something, something and maybe something
  */

// get stuff we need to get started //
const http = require( './lib/http' )

// initialise the http server //
http.init()
