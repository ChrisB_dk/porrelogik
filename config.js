#!/usr/bin/env node
'use strict'

/**
  *  configurations
  */

// import stuff we need //
const info = require( './lib/info')

// export our setttings //
module.exports = {
  http: {
    'hOst': process.env.HOST || '0.0.0.0',
    'pOrt': process.env.PORT || 8080
  },
  html: {
    'tItle': 'Robot Remote Cuntroll',
    'pAth': './assets/',
    'bOdy': 'body.html',
    'sTyle': 'main.css',
    'sCript': 'main.js'
  },
  wire: {
    'mOde': 'wpi',
    'pIn': 100
  }
}
